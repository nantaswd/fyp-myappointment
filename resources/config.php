<?php
/*  Main Configuration File: Creating Constants For Heavily Used Paths */
define("COMPONENTS_PATH", realpath(dirname(__FILE__) . '/components'));
define("ENTITIES_PATH", realpath(dirname(__FILE__) . '/entities'));
define("UTILS_PATH", realpath(dirname(__FILE__) . '/utils'));
define("ENUMS_PATH", realpath(dirname(__FILE__) . '/enums'));
define("FUNCTIONS_PATH", realpath(dirname(__FILE__) . '/functions'));
?>

