<?php
/*
 * @author yanying (Tracy)
 */
abstract class Appointment_Status {

    // -- Status Visible To Patient -- //
    const MISSED = "Missed";
    const UPCOMING = "Upcoming";
    
    // -- Status Not Visible To Patient -- //
    const COMPLETED = "Completed";
    const CANCELLED = "Cancelled";

}
?>