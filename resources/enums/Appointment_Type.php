<?php
/*
 * @author yanying (Tracie)
 */
abstract class Appointment_Type {

    const SPECIALIST_CONSULTATION = "Specialist Consultation";
    const DOCTOR_CONSULTATION = "Doctor Consultation";
    const CHECK_UP = "Check Up";

}
?>